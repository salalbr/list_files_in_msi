'listmsifiles.vbs
'	Usage: listmsifiles.vbs [<Folder containing MSI files> OR <MSI file>]
'
'	Author: Maer Melo
'	Email:  salalbr@gmail.com
'	Date: 	Mar 20, 2012

Option Explicit
Dim objFSO, objStartFolder, objFolder, colFiles, objFile, installer, database, Record, View, logFile, WshShell
Const msiOpenDatabaseModeReadOnly = 0

'Setup Shell environment
Set WshShell = CreateObject("Wscript.Shell")
Set objFSO = CreateObject("Scripting.FileSystemObject")
objStartFolder = WScript.Arguments.Item(0)

'Setup MSI environment
Set installer = Nothing
Set installer = Wscript.CreateObject("WindowsInstaller.Installer")

'Setup output log file
Set logFile = objFSO.CreateTextFile(WshShell.CurrentDirectory & "\listmsifiles.txt", True)

If objFSO.FolderExists(WScript.Arguments.Item(0)) Then
	processFolder
Else
	If objFSO.FileExists(WScript.Arguments.Item(0)) Then
		processFile
	Else
		Wscript.Quit(0)
	End If
End If

Sub processFolder
	Set objFolder = objFSO.GetFolder(objStartFolder)
	Set colFiles = objFolder.Files

	For Each objFile in colFiles
		If objFSO.GetExtensionName(objFSO.GetFolder(objStartFolder) & "\" & objFile.Name) = "msi" Then
			logFile.WriteLine("--MSI: " & objFile.Name)
				
			Set database = installer.OpenDatabase(objFSO.GetFolder(objStartFolder) & "\" & objFile.Name, msiOpenDatabaseModeReadOnly)
			'You may include other columns
			Set View = database.OpenView("SELECT FileName FROM File")
			View.Execute
			Do
				Set Record = View.Fetch
				If Record Is Nothing Then Exit Do
				logFile.WriteLine(Record.StringData(1))
			Loop
			logFile.WriteLine
		End If
	Next
End Sub

Sub processFile
	logFile.WriteLine("--MSI: " & objStartFolder)
		
	Set database = installer.OpenDatabase(objStartFolder, msiOpenDatabaseModeReadOnly)
	'You may include other columns
	Set View = database.OpenView("SELECT FileName FROM File")
	View.Execute
	Do
		Set Record = View.Fetch
		If Record Is Nothing Then Exit Do
		logFile.WriteLine(Record.StringData(1))
	Loop
End Sub